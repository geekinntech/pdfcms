namespace PDFCMS.Migrations
{
    using Microsoft.AspNet.Identity;
    using PDFCMS.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PDFCMS.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(PDFCMS.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("secret@123");
            context.Users.AddOrUpdate(u => u.UserName,
                new ApplicationUser
                {
                    UserName = "admin",
                    PasswordHash = password
                });

            context.Categories.AddOrUpdate(
              c => c.Name,
              new Category { Name = "Institute Handbook", Identifier=1 },
              new Category { Name = "Fabrication Guides", Identifier = 2 },
              new Category { Name = "White Papers", Identifier = 3 },
              new Category { Name = "Resources", Identifier = 4 }
            );
        }
    }
}

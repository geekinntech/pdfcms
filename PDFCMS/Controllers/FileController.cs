﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PDFCMS.Models;
using System.IO;
using PagedList;
namespace PDFCMS.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /File/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var files = from s in db.Files
                             select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                files = files.Where(s => s.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    files= files.OrderByDescending(s => s.Name);
                    break;
                default:
                    files = files.OrderBy(s => s.Name);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<CategoryFile> filesList = files.ToList();
            foreach (CategoryFile file in filesList)
            {
                file.Category = db.Categories.Find(file.CategoryId);
            }
            return View(filesList.ToPagedList(pageNumber, pageSize));
        }
        [AllowAnonymous]
        public FileResult DownloadFile(string fileName)
        {
            var filepath = System.IO.Path.Combine(Server.MapPath("~/App_Data/uploads/category"), fileName);
            return File(filepath, MimeMapping.GetMimeMapping(filepath), fileName);
        }
        // GET: /File/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryFile file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // GET: /File/Create
        public ActionResult Create()
        {
            var model = new CategoryFile
            {
                Categories = db.Categories.ToList()
            };
            //List<SelectListItem> categories = new List<SelectListItem>();
            //foreach (var item in categories)
            //{
            //    categories.Add(new SelectListItem()
            //    {
            //        Text = item.,
            //        Value = item,
            //        Selected = (item == defaultCountry ? true : false)
            //    });
            //}
            return View(model);
        }
        // POST: /File/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        public ActionResult Create(CategoryFile catFile)
        {
            // Set up DataTable place holder

            catFile.Categories = db.Categories.ToList();

            if (catFile != null && catFile.File == null)
            {
                ViewData.ModelState.AddModelError("File", "Please upload a file.");
            }
            else
            {
                if (catFile.File.ContentLength > 50 * 1024 * 1024)
                {
                    ViewData.ModelState.AddModelError("File", "Maximum allowed file size is 50MB.");
                    return View(catFile);
                }
                string path = Path.Combine(Server.MapPath("~/App_Data/uploads/category"), Path.GetFileName(catFile.File.FileName));
                System.IO.FileInfo file = MakeUnique(path);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                if (ModelState.IsValid)
                {
                    if (catFile.File.FileName.EndsWith(".pdf"))
                    {
                        CategoryFile newfile = new CategoryFile();
                        string dir = Path.GetDirectoryName(file.FullName);
                        string fileName = Path.GetFileNameWithoutExtension(file.FullName);
                        string fileExt = Path.GetExtension(file.FullName);
                        newfile.Name = Path.Combine(fileName + fileExt);
                        catFile.File.SaveAs(file.FullName);
                        //newfile.Path = file.FullName;
                        newfile.Path = "/File/DownloadFile?fileName=" + newfile.Name;

                        newfile.Version = 1;
                        newfile.CategoryId = catFile.CategoryId;
                        newfile.pagesLimit = catFile.pagesLimit;
                        newfile.Title = catFile.Title;
                        db.Files.Add(newfile);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Only PDF file format is supported.");
                        return View(catFile);
                    }
                    
                }
            }
            return View(catFile);
        }

        public FileInfo MakeUnique(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string fileExt = Path.GetExtension(path);

            for (int i = 1; ; ++i)
            {
                if (!System.IO.File.Exists(path))
                    return new FileInfo(path);

                path = Path.Combine(dir, fileName + "_" + i + fileExt);
            }
        }
        [HttpPost]
        public ActionResult Edit(CategoryFile catFile)
        {
            catFile.Categories = db.Categories.ToList();
            // Set up DataTable place holder
            if (catFile != null && catFile.File == null)
            {
                //ViewData.ModelState.AddModelError("File", "Please upload a file.");
                CategoryFile editFile = db.Files.Find(catFile.FileId);
                if (editFile != null)
                { 
                    editFile.Title = catFile.Title;
                    editFile.CategoryId = catFile.CategoryId;
                    editFile.pagesLimit = catFile.pagesLimit;
                    db.Entry(editFile).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                if (catFile.File.ContentLength > 50 * 1024 * 1024)
                {
                    ViewData.ModelState.AddModelError("File", "Maximum allowed file size is 50MB.");
                    return View(catFile);
                }
                string path = Path.Combine(Server.MapPath("~/App_Data/uploads/category"), Path.GetFileName(catFile.File.FileName));
                System.IO.FileInfo file = MakeUnique(path);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                if (ModelState.IsValid)
                {
                    if (catFile.FileId > 0)
                    {
                        CategoryFile oldfile = db.Files.Find(catFile.FileId);
                        if (oldfile != null)
                        {
                            if (catFile.File.FileName.EndsWith(".pdf"))
                            {
                                string dir = Path.GetDirectoryName(file.FullName);
                                string fileName = Path.GetFileNameWithoutExtension(file.FullName);
                                string fileExt = Path.GetExtension(file.FullName);
                                oldfile.Name = Path.Combine(fileName + fileExt);
                                //oldfile.Path = file.FullName;
                                oldfile.Path = "/File/DownloadFile?fileName=" + oldfile.Name;
                                catFile.File.SaveAs(file.FullName);
                                oldfile.Version = oldfile.Version + 1;
                                oldfile.pagesLimit = catFile.pagesLimit;
                                oldfile.CategoryId = catFile.CategoryId;
                                db.Entry(oldfile).State = EntityState.Modified;
                                db.SaveChanges();
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("File", "Only PDF file format is supported.");
                                return View(catFile);
                            }
                        }
                    }
                }
            }
            return View(catFile);
        }
        // GET: /File/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryFile file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            file.Categories = db.Categories.ToList();
            return View(file);
        }


        // GET: /File/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryFile file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        // POST: /File/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public string DeleteConfirmed(int id)
        {
            string message = "Success";
            try
            {
                CategoryFile catFile = db.Files.Find(id);
                db.Files.Remove(catFile);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
            //return RedirectToAction("Index", new { fileId=fileId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

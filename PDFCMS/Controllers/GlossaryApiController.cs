﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;

namespace PDFCMS.Controllers
{
    public class GlossariesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/Glossaries
        [ResponseType(typeof(Glossary))]
        [HttpGet]
        public HttpResponseMessage index(string email, string token)
        {
            List<Subscriber> sub = db.Subscribers.Where(s => s.Email == email).ToList();
            if (sub.Count() == 0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            List<Glossary> glossary = db.Glossary.ToList();
            return Request.CreateResponse(HttpStatusCode.OK, glossary);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GlossaryExists(int id)
        {
            return db.Glossary.Count(e => e.GlossaryId == id) > 0;
        }
    }
}
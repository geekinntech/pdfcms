﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;

namespace PDFCMS.Controllers
{
    public class DownloadsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/DownloadApi
        //public IQueryable<DownloadedFile> GetDownloads()
        //{
        //    return db.DownloadedFiles;
        //}

        // POST api/DownloadApi
        [ResponseType(typeof(DownloadedFile))]
        public HttpResponseMessage PostDownloads(DownloadedFile DownloadedFile, string email, string token)
        {
            List<Subscriber> sub = db.Subscribers.Where(s => s.Email == email).ToList();
            if (sub.Count()==0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            DownloadedFile.SubscriberId = sub.First().SubscriberId;
            db.DownloadedFiles.Add(DownloadedFile);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, new { message= "Record added successfully.", success=true});
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DownloadedFilesExists(int id)
        {
            return db.DownloadedFiles.Count(e => e.DownloadId == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;
using System.Text;

namespace PDFCMS.Controllers
{
    public class SubscribersController : ApiController
    {
        private static Random random = new Random((int)DateTime.Now.Ticks);
        private ApplicationDbContext db = new ApplicationDbContext();

        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
        // POST api/Subscriber
        [HttpPost]
        [ResponseType(typeof(Subscriber))]
        public HttpResponseMessage Auth(Subscriber sub)
        {
            List<Subscriber> subs=db.Subscribers.Where(x => x.Email.Equals(sub.Email, StringComparison.InvariantCultureIgnoreCase)).ToList();
            Subscriber updatedSub = null;
            if (subs.Count() > 0)
            {
                updatedSub = subs.First();
                string randomStr = RandomString(24);
                updatedSub.token = randomStr;
                db.Entry(updatedSub).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                var message = string.Format("User not found");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            SubscriberReturnModel sr = new SubscriberReturnModel();
            sr.status = true;
            sr.Email = updatedSub.Email;
            sr.token = updatedSub.token;
            sr.message = "Success";
            return Request.CreateResponse(HttpStatusCode.OK, sr);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubscriberExists(int id)
        {
            return db.Subscribers.Count(e => e.SubscriberId == id) > 0;
        }
    }
}
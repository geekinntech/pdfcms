﻿using PDFCMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net;
using System.Data.Entity;
namespace PDFCMS.Controllers
{
    public class GlossaryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /Glossary/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var glossaries = from g in db.Glossary
                              select g;
            if (!String.IsNullOrEmpty(searchString))
            {
                glossaries = glossaries.Where(g => g.Word.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "alpha_desc":
                    glossaries = glossaries.OrderByDescending(g => g.Alphabet);
                    break;
                default:
                    glossaries = glossaries.OrderBy(g => g.Alphabet);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            //glossaries = glossaries.Where(t => t.FileId == fileId);
            return View(glossaries.ToPagedList(pageNumber, pageSize));
        }
        // GET: /Glossary/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Glossary/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] 
        public ActionResult Create(Glossary glossary)
        {

            if (ModelState.IsValid)
            {
                db.Glossary.Add(glossary);
                db.SaveChanges();
                return RedirectToAction("Index");
                
            }

            return View(glossary);
        }

        // GET: /Glossary/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Glossary glossary = db.Glossary.Find(id);
            if (glossary == null)
            {
                return HttpNotFound();
            }
            return View(glossary);
        }

        // POST: /Glossary/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] 
        public ActionResult Edit(Glossary glossary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(glossary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(glossary);
        }

        // GET: /Glossary/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Glossary glossary = db.Glossary.Find(id);
            if (glossary == null)
            {
                return HttpNotFound();
            }
            return View(glossary);
        }

        // POST: /Glossary/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public string DeleteConfirmed(int id)
        {
            string message = "Success";
            try
            {
                Glossary glossary = db.Glossary.Find(id);
                db.Glossary.Remove(glossary);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
            //return RedirectToAction("Index", new { fileId=fileId});
        }

        public ActionResult Import()
        {
            return View();
        }
        [HttpPost, ActionName("Import")]
        public ActionResult Import(Glossary glossary)
        {
            if (glossary != null && glossary.File == null)
            {
                ViewData.ModelState.AddModelError("File", "Please upload your file.");
            }
            else
            {
                // Set up DataTable place holder
                string fileName = Path.GetFileName(glossary.File.FileName);
                string path = Path.Combine(Server.MapPath("~/App_Data/uploads/glossary"), fileName);
                System.IO.FileInfo file = new System.IO.FileInfo(path);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                glossary.File.SaveAs(path);
                var glossaries = new List<Glossary>();
                if (glossary.File != null && glossary.File.ContentLength > 0)
                {
                    if (glossary.File.FileName.EndsWith(".csv"))
                    {
                        using (var rd = new StreamReader(path))
                        {
                            while (!rd.EndOfStream)
                            {
                                var line = rd.ReadLine();
                                string[] parts = line.Split('|');
                                for (int i = 0; i < parts.Length; i++)
                                {
                                    parts[i] = parts[i].Trim();
                                }
                                if (parts.Length == 3)
                                {
                                    //glossaries.Add(new Glossary { Alphabet = parts[0], Word = parts[1], Definition = parts[2],FileId=fileId });
                                    glossaries.Add(new Glossary { Alphabet = parts[0], Word = parts[1], Definition = parts[2] });
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "CSV file format is incorrect.");
                                    return View(glossary);
                                }
                            }
                        }
                        if (glossaries.Count > 0)
                        {
                            using (var context = new ApplicationDbContext())
                            {
                                using (var dbContextTransaction = context.Database.BeginTransaction())
                                {
                                    try
                                    {   // some stuff in dbcontext
                                        //string query = String.Format("DELETE FROM GLOSSARIES WHERE FILEID={0}",fileId);
                                        context.Database.ExecuteSqlCommand("TRUNCATE TABLE GLOSSARIES");
                                        //context.Database.ExecuteSqlCommand(query);
                                        int size = glossaries.Count;
                                        for (int i = 0; i < size; i++)
                                        {
                                            context.Glossary.Add(glossaries[i]);
                                        }

                                        context.SaveChanges();
                                        dbContextTransaction.Commit();
                                        //return RedirectToAction("Index", new { fileId = fileId });
                                        return RedirectToAction("Index");
                                    }
                                    catch (Exception ex)
                                    {
                                        dbContextTransaction.Rollback();
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Only CSV file format is supported.");
                        return View(glossary);
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please upload a CSV file.");
                }
            }
            return View(glossary);
        }
	}
}
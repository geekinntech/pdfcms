﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;
using System.Text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using iTextSharp.text;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;

namespace PDFCMS.Controllers
{
    public class FilesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/DownloadApi
        [HttpGet]
        public HttpResponseMessage index(string email, string token)
        {
            if (db.Subscribers.Where(s => s.Email == email).Count() == 0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            List<CategoryFile> allFiles = db.Files.ToList();
            List<CategoryFileReturn> filesToReturn = new List<CategoryFileReturn>();
            foreach (CategoryFile file in allFiles)
            {
                file.Category = db.Categories.Find(file.CategoryId);
                filesToReturn.Add(new CategoryFileReturn { Title = file.Title, Name = file.Name, Path = System.Configuration.ConfigurationManager.AppSettings["sitePath"] + file.Path, FileId = file.FileId, Toc_Version = file.Toc_Version, Version = file.Version, Category = file.Category, CategoryId = file.CategoryId, pagesLimit = file.pagesLimit });
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, filesToReturn);
        }

        [HttpGet]
        // GET api/FileApi
        public HttpResponseMessage Get(int id, string email, string token)
        {
            List<CategoryFileReturn> filesToReturn = new List<CategoryFileReturn>();
            if (db.Subscribers.Where(s => s.Email == email).Count() == 0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            List<Subscriber> subscribers=db.Subscribers.Where(s=>s.Email==email).ToList();
            if(subscribers.Count()>0 && id>0){
                Category cat = db.Categories.Where(c => c.Identifier == id).FirstOrDefault();
                List<CategoryFile> allFiles = db.Files.Where(f => f.CategoryId== cat.CategoryId).ToList();
                Subscriber subscriber=subscribers.First();
                
                foreach (CategoryFile file in allFiles)
                {
                    List<DownloadedFile> isDownloaded = db.DownloadedFiles.Where(d => d.SubscriberId == subscriber.SubscriberId && d.FileId==file.FileId).ToList();
                    if (isDownloaded != null && isDownloaded.Count()>0)
                    {
                        if (file.Version > isDownloaded.First().Version)
                        {
                            filesToReturn.Add(new CategoryFileReturn { Title=file.Title, Name=file.Name,Path=System.Configuration.ConfigurationManager.AppSettings["sitePath"]+file.Path,FileId=file.FileId, Toc_Version=file.Toc_Version, Version=file.Version,Category=file.Category,CategoryId=file.CategoryId, pagesLimit=file.pagesLimit});
                        }
                    }
                    else
                    {
                        filesToReturn.Add(new CategoryFileReturn { Title = file.Title, Name = file.Name, Path = System.Configuration.ConfigurationManager.AppSettings["sitePath"]+file.Path, FileId = file.FileId, Toc_Version = file.Toc_Version, Version = file.Version, Category = file.Category, CategoryId = file.CategoryId, pagesLimit = file.pagesLimit });
                    }
                } 
            }

            return Request.CreateResponse(HttpStatusCode.OK, filesToReturn);
        }

        private String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
        [HttpPost]
        [ResponseType(typeof(EmailFile))]
        // POST api/GetPages
        public HttpResponseMessage sendemail(EmailFile emailfile,string email, string token)
        {
            if (db.Subscribers.Where(s => s.Email == email).Count() == 0 || emailfile.EmailTo==null || emailfile.FileId==null || emailfile.Pages==null || emailfile.Pages.Length<1)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            CategoryFile inputFile = db.Files.Find(emailfile.FileId);
            string inputFilePath = String.Empty;
            if (inputFile != null)
            {
                inputFilePath = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/uploads/category"),inputFile.Name);
                string outputFilePath = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/uploads/attachments"), "Email-" + GetTimestamp(DateTime.Now) + ".pdf");
                System.IO.FileInfo file = new System.IO.FileInfo(outputFilePath);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                if (ExtractPages(inputFilePath, outputFilePath, emailfile.Pages))
                {
                    if (email_send(System.Configuration.ConfigurationManager.AppSettings["fromEmailAddress"], emailfile, outputFilePath, System.Configuration.ConfigurationManager.AppSettings["credentials"]))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,"Email Sent");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Something Went Wrong While Sending Email.");
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound,"Something Went Wrong While Extracting Pages, May Be The Inputs Are Wrong.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound,"File Not Found");
            }
            
            
        }

        private bool email_send(string from,EmailFile emailfile,string attachmentPath,string password)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtpHost"]);
                mail.From = new MailAddress(from);
                mail.To.Add(emailfile.EmailTo);
                mail.Subject = emailfile.Subject;
                string pagesStr = string.Empty;
                foreach (int page in emailfile.Pages)
                {
                    pagesStr += page + ",";
                }
                mail.Body = "Message: " + emailfile.Body + "\n" + "Name: " + emailfile.Name + "\n" + "Pages: " + pagesStr;



                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(attachmentPath);
                mail.Attachments.Add(attachment);

                SmtpServer.Port = int.Parse(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]);
                SmtpServer.Credentials = new System.Net.NetworkCredential(from, password);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ExtractPages(string sourcePdfPath, string outputPdfPath, int[] extractThesePages)
        {
            PdfReader reader = null;
            Document sourceDocument = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage = null;

            try
            {
                // Intialize a new PdfReader instance with the 
                // contents of the source Pdf file:
                reader = new PdfReader(sourcePdfPath);

                // For simplicity, I am assuming all the pages share the same size
                // and rotation as the first page:
                sourceDocument = new Document(reader.GetPageSizeWithRotation(extractThesePages[0]));

                // Initialize an instance of the PdfCopyClass with the source 
                // document and an output file stream:
                pdfCopyProvider = new PdfCopy(sourceDocument,
                    new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));

                sourceDocument.Open();

                // Walk the array and add the page copies to the output file:
                foreach (int pageNumber in extractThesePages)
                {
                    importedPage = pdfCopyProvider.GetImportedPage(reader, pageNumber);
                    pdfCopyProvider.AddPage(importedPage);
                }
                sourceDocument.Close();
                reader.Close();
                return true;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }
    }
}
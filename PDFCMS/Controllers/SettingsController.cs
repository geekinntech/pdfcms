﻿using PDFCMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PDFCMS.Controllers
{
    public class SettingsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /Settings/
        public ActionResult Index()
        {
            Settings s = db.Settings.FirstOrDefault();
            if (s != null)
            {
                return View(s);
            }
            s = new Settings();
            return View(s);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Settings settings)
        {
            if (ModelState.IsValid)
            {
                if (settings.SettingId > 0)
                {
                    Settings editSettings = db.Settings.Find(settings.SettingId);
                    editSettings.pagesLimit = settings.pagesLimit;
                    db.Entry(editSettings).State = EntityState.Modified;
                }
                else
                {
                    db.Settings.Add(settings);
                }
                db.SaveChanges();
            }
            else
            {
                return View("Index",settings);
            }
            return RedirectToAction("Index","Subscriber");
        }
	}
}
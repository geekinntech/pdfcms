﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;
using System.Web.Mvc;

namespace PDFCMS.Controllers
{
    public class SettingController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/SettingsAPI
        public HttpResponseMessage GetSettings(string email, string token)
        {
            if (db.Subscribers.Where(s => s.Email == email).Count() == 0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, db.Settings.FirstOrDefault());
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SettingsExists(int id)
        {
            return db.Settings.Count(e => e.SettingId == id) > 0;
        }
    }
}
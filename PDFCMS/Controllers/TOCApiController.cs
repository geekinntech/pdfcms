﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PDFCMS.Models;

namespace PDFCMS.Controllers
{
    public class TOCsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/TopicApi/5
        [ResponseType(typeof(TOC))]
        public HttpResponseMessage Get(int id, string email, string token)
        {
            List<Subscriber> sub = db.Subscribers.Where(s => s.Email == email).ToList();
            if (sub.Count() == 0)
            {
                var message = string.Format("Bad Request");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
            List<TOC> tocs = db.Tocs.Where(t=>t.FileId==id && t.ParentId==null).ToList();
            List<TOCReturn> result = createItemArray(tocs);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private List<TOCReturn> createItemArray(List<TOC> tocs)
        {
            List<TOCReturn> result = new List<TOCReturn>();
            foreach(TOC toc in tocs)
            {
                TOCReturn newToc = new TOCReturn()
                {
                    TopicId = toc.TopicId,
                    Name = toc.Name,
                    PageNo = toc.PageNo,
                    ParentId = toc.ParentId,
                    FileId=toc.FileId
                };
                List<TOCReturn> children = itemWithChildren(newToc);
                newToc.Children = children;
                newToc.File = db.Files.Find(newToc.FileId);
                result.Add(newToc);

            }
            return result;
        }
        private List<TOCReturn> itemWithChildren(TOCReturn item)
        {
            List<TOCReturn> result = new List<TOCReturn>();
            List<TOCReturn> children = childrenOf(item);
            foreach (TOCReturn child in children)
            {
                List<TOCReturn> furtherChildren = itemWithChildren(child);
                child.Children = furtherChildren;
                result.Add(child);
            }
            return result;
        }

        private List<TOCReturn> childrenOf(TOCReturn item)
        {
            List<TOCReturn> tocReturn = new List<TOCReturn>();
            List<TOC> tocs = db.Tocs.Where(t => t.ParentId == item.TopicId).ToList();
            foreach (TOC toc in tocs)
            {
                TOCReturn newToc = new TOCReturn()
                {
                    TopicId = toc.TopicId,
                    Name = toc.Name,
                    PageNo = toc.PageNo,
                    ParentId = toc.ParentId,
                    FileId = toc.FileId
                };
                tocReturn.Add(newToc);

            }
            return tocReturn;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TOCExists(int id)
        {
            return db.Tocs.Count(e => e.TopicId == id) > 0;
        }
    }
}
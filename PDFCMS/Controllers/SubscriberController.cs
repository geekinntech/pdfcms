﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PDFCMS.Models;
using System.IO;
using System.Text.RegularExpressions;
using PagedList;

namespace PDFCMS.Controllers
{
    [Authorize]
    public class SubscriberController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Subscriber/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "email_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var subscribers = from s in db.Subscribers
                             select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                subscribers = subscribers.Where(s => s.Email.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "email_desc":
                    subscribers = subscribers.OrderByDescending(s => s.Email);
                    break;
                default:
                    subscribers = subscribers.OrderBy(s => s.Email);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(subscribers.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Subscriber/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = db.Subscribers.Find(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            return View(subscriber);
        }

        // GET: /Subscriber/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Subscriber/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SubscriberId,Email")] Subscriber subscriber)
        {

            if (ModelState.IsValid)
            {
                if (db.Subscribers.Where(x => x.Email.Equals(subscriber.Email, StringComparison.InvariantCultureIgnoreCase)).Count() > 0)
                {
                    ViewData.ModelState.AddModelError("Email", "Email has already been subscribed.");
                }
                else
                {
                    db.Subscribers.Add(subscriber);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(subscriber);
        }

        // GET: /Subscriber/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = db.Subscribers.Find(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            return View(subscriber);
        }

        // POST: /Subscriber/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SubscriberId,Email")] Subscriber subscriber)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subscriber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subscriber);
        }

        // GET: /Subscriber/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = db.Subscribers.Find(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            return View(subscriber);
        }

        // POST: /Subscriber/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public string DeleteConfirmed(int id)
        {
            string message = "Success";
            try
            {
                Subscriber sub= db.Subscribers.Find(id);
                db.Subscribers.Remove(sub);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                message = "Failure";
            }
            return message;
            //return RedirectToAction("Index", new { fileId=fileId});
        }
        public ActionResult Import()
        {
            return View();
        }
        [HttpPost, ActionName("Import")]
        public ActionResult Import(Subscriber subscriber)
        {
            if (subscriber != null && subscriber.File == null)
            {
                ViewData.ModelState.AddModelError("File", "Please upload your file.");
            }
            else
            {
                // Set up DataTable place holder
                string fileName = Path.GetFileName(subscriber.File.FileName);
                string path = Path.Combine(Server.MapPath("~/App_Data/uploads/subscriber"), fileName);
                System.IO.FileInfo file = new System.IO.FileInfo(path);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                subscriber.File.SaveAs(path);
                var subscribers = new List<Subscriber>();
                if (subscriber.File != null && subscriber.File.ContentLength > 0)
                {
                    if (subscriber.File.FileName.EndsWith(".csv"))
                    {
                        using (var rd = new StreamReader(path))
                        {
                            while (!rd.EndOfStream)
                            {
                                var line = rd.ReadLine();
                                subscribers.Add(new Subscriber { Email = line });
                            }
                        }
                        if (subscribers.Count > 0)
                        {
                            using (var context = new ApplicationDbContext())
                            {
                                using (var dbContextTransaction = context.Database.BeginTransaction())
                                {
                                    try
                                    {   // some stuff in dbcontext
                                        //context.Database.ExecuteSqlCommand("TRUNCATE TABLE SUBSCRIBERS");
                                        int size = subscribers.Count;
                                        for (int i = 0; i < size; i++)
                                        {
                                            string emailtofind=subscribers[i].Email;
                                            List<Subscriber> storedSub = db.Subscribers.Where(x => x.Email == emailtofind).ToList();
                                            if (storedSub.Count() == 0)
                                            {
                                                context.Subscribers.Add(subscribers[i]);
                                            }
                                        }

                                        context.SaveChanges();
                                        dbContextTransaction.Commit();
                                        return RedirectToAction("Index");
                                    }
                                    catch (Exception ex)
                                    {
                                        dbContextTransaction.Rollback();
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Only CSV file format is supported.");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please upload a CSV file.");
                }
            }
            return View(subscriber);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

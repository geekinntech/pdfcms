﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PDFCMS.Models;
using PagedList;
namespace PDFCMS.Controllers
{
    public class TOCController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /TOC/
        public ActionResult Index(int fileId, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.PageSortParm = String.IsNullOrEmpty(sortOrder) ? "page" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var Tocs = from s in db.Tocs
                        select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                Tocs = Tocs.Where(s => s.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "page":
                    Tocs = Tocs.OrderBy(s => s.PageNo);
                    break;
                default:
                    Tocs = Tocs.OrderBy(s => s.PageNo);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<TOC> tocList = Tocs.Where(t=>t.FileId==fileId).ToList();
            foreach (TOC toc in tocList)
            {
                if (toc.ParentId != null && toc.ParentId > 0)
                {
                    toc.Parent = db.Tocs.Find(toc.ParentId);
                }
            } 
            return View(tocList.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: /TOC/Create
        public ActionResult Create(int fileId)
        {
            var model = new TOC
            {
                Topics = db.Tocs.Where(t=>t.FileId==fileId).ToList(),
                FileId=fileId,
                ParentId=0
            };
            return View(model);
        }

        // POST: /TOC/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TOC topic)
        {
            if (ModelState.IsValid)
            {
                CategoryFile editFile = db.Files.Find(topic.FileId);
                if (editFile != null)
                {
                    editFile.Toc_Version = editFile.Toc_Version + 1;
                    db.Entry(editFile).State = EntityState.Modified;
                    db.SaveChanges();
                }
                db.Tocs.Add(topic);
                db.SaveChanges();
                return RedirectToAction("Index", new { fileId = topic.FileId });
            }
            topic.Topics = db.Tocs.ToList();
            return View(topic);
        }

        // GET: /TOC/Edit/5
        public ActionResult Edit(int? id,int fileId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TOC topic = db.Tocs.Find(id);
            if (topic== null)
            {
                return HttpNotFound();
            }
            topic.Topics = db.Tocs.Where(t => t.TopicId != id && t.FileId == fileId).ToList();
            topic.FileId = fileId;
            
            return View(topic);
        }

        // POST: /TOC/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TOC topic)
        {
            if (ModelState.IsValid)
            {
                CategoryFile editFile = db.Files.Find(topic.FileId);
                if (editFile != null)
                {
                    editFile.Toc_Version = editFile.Toc_Version + 1;
                    db.Entry(editFile).State = EntityState.Modified;
                    db.SaveChanges();
                }
                db.Entry(topic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { fileId = topic.FileId });
            }
            topic.Topics = db.Tocs.Where(t => t.TopicId != topic.TopicId && t.FileId==topic.FileId).ToList();
            topic.FileId = topic.FileId;
            return View(topic);
        }

        // GET: /TOC/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TOC TOC = db.Tocs.Find(id);
            if (TOC == null)
            {
                return HttpNotFound();
            }
            return View(TOC);
        }

        // POST: /TOC/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public string DeleteConfirmed(int id)
        {
            string message="Success";
            try{
                TOC TOC = db.Tocs.Find(id);
                CategoryFile editFile = db.Files.Find(TOC.FileId);
                if (editFile != null)
                {
                    editFile.Toc_Version = editFile.Toc_Version + 1;
                    db.Entry(editFile).State = EntityState.Modified;
                    db.SaveChanges();
                }
                db.Tocs.Remove(TOC);
                db.SaveChanges();
            }
            catch(Exception ex){
                message="Failure";
            }
            return message;
            //return RedirectToAction("Index", new { fileId=fileId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

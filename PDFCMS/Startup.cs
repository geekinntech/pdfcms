﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PDFCMS.Startup))]
namespace PDFCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

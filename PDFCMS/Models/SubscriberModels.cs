﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class Subscriber
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubscriberId { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is invalid")]
        public string Email { get; set; }
        public string token { get; set; }
        [NotMapped]
        public HttpPostedFileBase File { get; set; }
    }


    public class SubscriberReturnModel
    {
        public bool status { get; set; }
        public string Email { get; set; }
        public string token { get; set; }

        public string message { get; set; }

        public SubscriberReturnModel()
        {
            status = false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class CategoryFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FileId { get; set; }
        [Required]
        public string Title { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public int Version { get; set; }

        public int Toc_Version { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public Category Category { get; set; }

        [Required]
        [Display(Name = "Email Pages Limit")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Please enter a non negative number.")]
        public int pagesLimit { get; set; }
        public IEnumerable<Category> Categories{ get; set; }

        [NotMapped]
        //[MaxFileSize(50, ErrorMessage = "Maximum allowed file size is {0} MB")]
        public HttpPostedFileBase File { get; set; }

        public CategoryFile()
        {
            Toc_Version = 0;
        }

    }
    public class CategoryFileReturn
    {
        public int FileId { get; set; }
        [Required]
        public string Title { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public int Version { get; set; }
        public int Toc_Version { get; set; }

        public int pagesLimit { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
    public class EmailFile
    {
        public int FileId { get; set; }
        public int Version { get; set; }
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public int[] Pages { get; set; }
    }
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxFileSize;
        public MaxFileSizeAttribute(int maxFileSizeMBs)
        {
            _maxFileSize = maxFileSizeMBs * 1024 * 1024;
        }

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
            {
                return false;
            }
            return file.ContentLength <= _maxFileSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage((_maxFileSize/1048576).ToString());
        }
    }
}
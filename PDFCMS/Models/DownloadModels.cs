﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class DownloadedFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DownloadId { get; set; }

        public int FileId { get; set; }

        public int SubscriberId { get; set; }

        public int Version { get; set; }
    }
}
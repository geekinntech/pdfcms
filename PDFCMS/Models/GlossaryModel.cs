﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class Glossary
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GlossaryId { get; set; }

        [Required]
        [Display(Name = "Alphabet")]
        public string Alphabet { get; set; }

        [Required]
        [Display(Name = "Word")]
        public string Word { get; set; }
        [Required]
        [Display(Name = "Definition")]
        public string Definition { get; set; }
        [NotMapped]
        public HttpPostedFileBase File { get; set; }
    }

}
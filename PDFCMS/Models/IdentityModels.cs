﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace PDFCMS.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("pdfcms")
        {
        }

        public System.Data.Entity.DbSet<PDFCMS.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.Subscriber> Subscribers { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.CategoryFile> Files { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.DownloadedFile> DownloadedFiles { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.Glossary> Glossary { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.TOC> Tocs { get; set; }

        public System.Data.Entity.DbSet<PDFCMS.Models.Settings> Settings { get; set; }
    }
}
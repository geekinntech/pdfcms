﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class TOC
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TopicId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Page No.")]
        [Range(1, Int32.MaxValue)]
        public int PageNo{ get; set; }
        public int FileId { get; set; }
        public int? ParentId { get; set; }
        [NotMapped]
        public ICollection<TOC> Topics { get; set; }
        [NotMapped]
        public TOC Parent { get; set; }
        public TOC()
        {
            PageNo = 1;
            ParentId = 0;
        }
    }


    public class TOCReturn
    {
        public int TopicId { get; set; }
        public string Name { get; set; }
        public int PageNo { get; set; }
        public int? ParentId { get; set; }
        public int FileId { get; set; }
        public CategoryFile File{get; set; }
        
        public ICollection<TOCReturn> Children { get; set; }
    }
}
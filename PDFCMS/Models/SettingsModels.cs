﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PDFCMS.Models
{
    public class Settings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SettingId { get; set; }

        [Required]
        [Display(Name = "Email Pages Limit")]
        [Range(0, Int32.MaxValue, ErrorMessage="Please enter a non negative number.")]
        public int pagesLimit { get; set; }

        public Settings()
        {
            SettingId = 0;
        }


    }
}